let feedbackImages = document.querySelectorAll('.feedback-block');
let next = document.querySelector('.next');
let prev = document.querySelector('.prev');
let dots = document.querySelectorAll('.dot');
let counter = 0;

//buttons next and prev
next.addEventListener('click', feedbackNext);
function feedbackNext(){
    feedbackImages[counter].style.animation = 'next1 0.5s ease-in forwards';
    if(counter >= feedbackImages.length-1){
        counter = 0
    }
    else{
        counter++
    }
    feedbackImages[counter].style.animation = 'next2 0.5s ease-in forwards';
    indicators()
}

prev.addEventListener('click', feedbackPrev);
function feedbackPrev(){
    feedbackImages[counter].style.animation = 'prev1 0.5s ease-in forwards';
    if(counter === 0){
        counter = feedbackImages.length-1
    }
    else{
        counter--
    }
    feedbackImages[counter].style.animation = 'prev2 0.5s ease-in forwards';
    indicators()
}

function autoSliding(){
    deleteInterval = setInterval(timer, 4000);
    function timer(){
        feedbackNext();
        indicators();
    }
}
autoSliding()

//stop and resume auto sliding 
const container = document.querySelector('.feedback-container');
container.addEventListener('mouseover', function(){
    clearInterval(deleteInterval)
})

container.addEventListener('mouseout', autoSliding);

//add and remove active class from  mini images
function indicators(){
    for(i = 0; i < dots.length; i++){
        dots[i].className = dots[i].className.replace(' active', '')
    }
    dots[counter].className += ' active';

}

function switchImage(currentImage){
    currentImage.classList.add('active');
    let imageId = currentImage.getAttribute('attr');
    if(imageId > counter){
        feedbackImages[counter].style.animation = 'next1 0.5s ease-in forwards';
        counter = imageId;
        feedbackImages[counter].style.animation = 'next2 0.5s ease-in forwards';
    }
    else if(imageId === counter){
        return;
    }
    else{
        feedbackImages[counter].style.animation = 'prev1 0.5s ease-in forwards';
        counter = imageId;
        feedbackImages[counter].style.animation = 'prev2 0.5s ease-in forwards';
    }
    indicators()
}